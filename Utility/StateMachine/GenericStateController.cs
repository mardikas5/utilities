﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenericStateController : MonoBehaviour
{
    public GenericState ActiveState => _activeState;

    [SerializeField]
    protected GenericState _activeState;

    [SerializeField]
    protected GenericState startState;

    public Dictionary<string, int> ActionInts = new Dictionary<string, int>();

    public event Action<int, object> ActionSignal;

    public event Action<GenericState> stateChange;

    private int ActionCounter = -1;

    public List<GenericState> stateBehaviours = new List<GenericState>();

    public void Restart()
    {
        SetState(startState);
    }

    public void Awake()
    {
        stateBehaviours.Clear();

        GenericState[] states = transform.GetComponentsInChildren<GenericState>(true);

        stateBehaviours.AddRange(states);

        foreach (GenericState state in stateBehaviours)
        {
            state.Init(this);
        }

        SetState(startState);
    }

    public void Update()
    {
        foreach (GenericState state in stateBehaviours)
        {
            state.OnPreUpdate();
        }

        ActiveState?.UpdateBehaviour();
    }

    public int GetAction(string name)
    {
        if (ActionInts.TryGetValue(name, out int _value))
        {
            return _value;
        }
        else
        {
            return RegisterAction(name);
        }
    }


    public void RegisterTrigger<T, T1>(T handler, Func<bool> preCheck = null) where T : Delegate
    {
        int actionID = GetAction(GenericString<T1>());
        Action<int, object> attachedAction = (x, y) =>
        {
            if (x != actionID)
            {
                return;
            }

            if (preCheck != null)
            {
                if (preCheck?.Invoke() == false)
                {
                    return;
                }
            }

            handler.DynamicInvoke(y);
        };

        ActionSignal += attachedAction;
    }


    /// <summary>
    /// Register a strongly typed action.
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public int RegisterAction<T>()
    {
        return RegisterAction(GenericString<T>());
    }

    /// <summary>
    /// Register a very generic action. -> no restrictions on input.
    /// String action, returns int for when that action is signalled.</summary>
    /// int is a faster comparison checking later than a string.
    /// <param name="action"></param>
    /// <returns></returns>
    public int RegisterAction(string action)
    {
        if (ActionInts.TryGetValue(action, out int _value))
        {
            return _value;
        }

        ActionCounter++;

        ActionInts.Add(action, ActionCounter);
        return ActionCounter;
    }

    public bool UnregisterAction(string action)
    {
        return ActionInts.Remove(action);
    }

    public bool SignalAction<T>(object param)
    {
        if (ActionInts.TryGetValue(GenericString<T>(), out int _value))
        {
            ActionSignal?.Invoke(_value, param);
            return true;
        }
        Debug.LogError(" failed to signal: " );
        return false;
    }

    public bool SignalAction(string action, params object[] input)
    {
        if (ActionInts.TryGetValue(action, out int _value))
        {
            ActionSignal?.Invoke(_value, input);
            return true;
        }

        return false;
    }

    /// <summary>
    /// Sets the current state in the statemachine.
    /// Use <see cref="GenericState.SwitchState(GenericState, bool)"/> to change states with entry conditions.
    /// </summary>
    /// <param name="newState"></param>
    public void SetState(GenericState newState)
    {
        if (newState == _activeState)
        {
            Debug.LogWarning("!! state set to already active state.");
        }

        //if state needs exit time ?? ..
        if (_activeState != null)
        {
            _activeState.ExitState();
        }

        _activeState = newState;

        stateChange?.Invoke(newState);

        if (newState != null)
        {
            newState.EnterState();
        }
        else
        {
            Debug.LogWarning("!! set state to null.");
        }
    }


    private string GenericString<T>()
    {
        int nr = typeof(T).GetHashCode();
        return nr.ToString();
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class GenericState : MonoBehaviour
{
    public GenericStateController _controller { get; private set; }

    public List<Func<bool>> EntryConditions = new List<Func<bool>>();

    public bool Active { get; private set; }
    public bool IsBusy => EvaluateBusy();

    public virtual void EnterState() { OnEnterState?.Invoke(); }

    public virtual void ExitState() { OnExitState?.Invoke(); }

    protected virtual void Update() { }

    /// <summary>
    /// Called every update frame on every state, right before <see cref="UpdateBehaviour"/>, even those not active.
    /// </summary>
    public virtual void OnPreUpdate() { }

    /// <summary>
    /// Main update function for the state, equal to a update tick.
    /// </summary>
    public virtual void UpdateBehaviour() { }

    public UnityEvent OnEnterState, OnExitState;
    
    public virtual void Init(GenericStateController controller)
    {
        _controller = controller;

        _controller.stateChange += OnStateChanged;
    }

    protected virtual bool EvaluateBusy()
    {
        return false;
    }

    protected virtual void OnStateChanged(GenericState obj)
    {
        if (obj == this)
        {
            Active = true;
            return;
        }

        Active = false;
    }

    protected virtual void SwitchState(GenericState newState, bool checkEntry = false)
    {
        if (checkEntry && !newState.CanEnter())
        {
            return;
        }

        _controller.SetState(newState);
    }

    private bool CanEnter()
    {
        foreach (Func<bool> t in EntryConditions)
        {
            if (!t.Invoke())
            { return false; }
        }
        return true;
    }

    /// <summary>
    /// Register a method handler for a generic signal, called on the statemachine.
    /// Example: OnCharacterMoveSignal as signal, with a OnCharacterMoveHandler as handler function.
    /// Used when a state can react to an event.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <param name="handler"></param>
    protected virtual void RegisterTrigger<T, T1>(T handler) where T : Delegate
    {
        Func<bool> preCheck = () =>
        {
            if (Active)
            {
                return true;
            }

            return false;
        };

        _controller.RegisterTrigger<T, T1>(handler, preCheck);
    }
}

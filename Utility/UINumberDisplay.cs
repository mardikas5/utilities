﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINumberDisplay
{
    public Tween activeTween;
    public float UIAmount;
    public float AmountMultiplier = 2f;
    public float BaseDuration = .5f;

    public string AmountString => UIAmount.ToString("F0");

    public event Action OnUpdate;

    private MonoBehaviour _mono;

    public UINumberDisplay Init(MonoBehaviour mono, Func<float> getter)
    {
        _mono = mono;
        mono.StartCoroutine(GetValue(getter));
        return this;
    }

    private IEnumerator GetValue(Func<float> getter)
    {
        WaitForFixedUpdate frame = new WaitForFixedUpdate();
        while (true)
        {
            if (getter == null)
            {
                break;
            }

            float value = getter();
            if (value != UIAmount)
            {
                OnMoneyUpdated(value);
            }

            yield return frame;
        }
    }

    private void OnMoneyUpdated(float endValue)
    {
        float difference = Mathf.Abs(UIAmount - endValue);
        float duration = BaseDuration + (AmountMultiplier * Mathf.Log10(difference));

        if (activeTween != null)
        {
            activeTween.Kill(false);
        }

        activeTween = DOTween.To(() => UIAmount, OnUpdateMoney, endValue, duration).SetEase(Ease.OutExpo).OnComplete(() =>
        {
            activeTween = null;
        });
    }

    public void DoSignal()
    {
        OnMoneyUpdated(UIAmount);
    }

    private void OnUpdateMoney(float pNewValue)
    {
        UIAmount = pNewValue;
        OnUpdate?.Invoke();
    }
}

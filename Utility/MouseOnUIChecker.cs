﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOnUIChecker : MonoBehaviour
{
    public bool LastFrameOnUI = false;

    public List<RaycastResult> lastFrameHits = new List<RaycastResult>();
    public GameObject hitUIObject;

    private bool checkedThisFrame = false;
    private bool IsOnUI = false;

    public int FramesOnUI = 0;
    public int FramesOffUI = 0;



    public void Update()
    {
        LastFrameOnUI = IsOnUI;

        IsOnUI = false;
        checkedThisFrame = false;

        MouseOnUI();

        if (IsOnUI)
        {
            FramesOnUI++;
            FramesOffUI = 0;
        }
        else
        {
            FramesOffUI++;
            FramesOnUI = 0;
        }
    }

    public bool MouseOnUI(bool countLastFrame = true)
    {
        if (checkedThisFrame)
        {
            if (IsOnUI)
            {
                return true;
            }
        }

        bool thisFrameOnUI = MouseOnUI(out List<RaycastResult> results);

        if (countLastFrame)
        {
            if (LastFrameOnUI)
            {
                return true;
            }
        }

        return thisFrameOnUI;
    }


    public bool MouseOnUI(out List<RaycastResult> results)
    {
        results = new List<RaycastResult>();

        PointerEventData pdata = new PointerEventData(EventSystem.current);
        pdata.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pdata, results);

        checkedThisFrame = true;

        lastFrameHits = results;

        foreach (RaycastResult t in results)
        {
            if (t.gameObject.layer == LayerMask.NameToLayer("UI"))
            {
                hitUIObject = t.gameObject;

                IsOnUI = true;
                return true;
            }
        }

        return false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2Utility
{
    public static Vector3 x0y( this Vector2 v )
    {
        return new Vector3( v.x, 0.0f, v.y );
    }

    public static Vector2 Abs( this Vector2 v )
    {
        return new Vector2( Mathf.Abs( v.x ), Mathf.Abs( v.y ) );
    }

    public static float Cross( this Vector2 a, Vector2 b )
    {
        return ( a.x * b.y ) - ( a.y * b.x );
    }

    public static Vector2 Div( this Vector2 a, Vector2 b )
    {
        return new Vector2( a.x / b.x, a.y / b.y );
    }

    public static Vector2 Mul( this Vector2 a, Vector2 b )
    {
        return new Vector2( a.x * b.x, a.y * b.y );
    }

    /// <summary>
    /// Returns <see cref="Random.Range(float, float)"/> value for x as min and y as maximum.
    /// </summary>
    /// <param name="limits"></param>
    /// <returns></returns>
    public static float Rand( this Vector2 limits)
    {
        return Random.Range(limits.x, limits.y);
    }
}

﻿using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Trees;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CombatTree : TreeBaseAI, ITreeProvider
{
    public Transform MovementTarget;

    public float CheckRadius = 10f;

    public GameObject _Enemy;

    public override string RootName => "CombatAI";

    private void Start()
    {
        MovementTarget = new GameObject("c_MoveTarget").transform;
    }

    protected override BehaviorTreeBuilder TreeBuilder(string name = "combat tree")
    {
        BehaviorTreeBuilder k = base.TreeBuilder(name);
        k.Name("Combat tree")
        .Sequence(" Move to enemy ")
            .Do("Get Target", GetTarget)
            .RepeatUntilSuccess("DoMove")
                .AgentDestination("Set Move to: ", () => MovementTarget.position)
                .Do("Moving", CheckDistanceStatus)
                .End()
            .End();
        return k;
    }

    private TaskStatus GetTarget()
    {
        List<Entity> targets = CheckForTargets();

        _Enemy = getClosest(targets)?.gameObject;

        if (_Enemy == null)
        {
            return TaskStatus.Failure;
        }

        MovementTarget.transform.position = _Enemy.transform.position;

        return TaskStatus.Success;
    }

    public List<Entity> CheckForTargets()
    {
        List<Entity> targets = new List<Entity>();
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, CheckRadius);
        Entity Controller = gameObject.transform.root.GetComponentInChildren<Entity>();

        foreach (Collider2D k in cols)
        {
            Entity charC = k?.attachedRigidbody?.GetComponentInChildren<Entity>();
            if (charC == null)
            {
                charC = k.GetComponentInChildren<Entity>();
            }

            if (charC == null)
            {
                continue;
            }
            if (Controller.Get<TeamDefinition>().Team == charC.Get<TeamDefinition>().Team)
            {
                continue;
            }

            targets.Add(charC);
        }

        return targets;
    }

    protected virtual Entity getClosest(List<Entity> chars)
    {
        return chars?.OrderBy((x) => Vector3.Distance(transform.position, x.transform.position)).FirstOrDefault();
    }

    private bool CheckDistance()
    {
        return Vector3.Distance(MovementTarget.transform.position, transform.position) < 2f;
    }

    private TaskStatus CheckDistanceStatus()
    {
        if (CheckDistance())
        {
            return TaskStatus.Success;
        }

        return TaskStatus.Continue;
    }
}

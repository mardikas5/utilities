﻿using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Tasks.Actions;
using System;
using UnityEngine;
using UnityEngine.AI;

public class AgentDestination : ActionBase
{
    private NavMeshAgent _agent;
    public Func<Vector3> Target;

    protected override void OnInit()
    {
        _agent = Owner.transform.root.GetComponent<NavMeshAgent>();
    }

    protected override TaskStatus OnUpdate()
    {
        if (Target != null)
        {
            Vector3 pos = Target.Invoke();
            _agent.SetDestination(pos);
            return TaskStatus.Success;
        }

        return TaskStatus.Failure;
    }
}
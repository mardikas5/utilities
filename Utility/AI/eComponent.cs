﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Entity Component
/// </summary>
[Serializable]
public class eComponent
{
    public Entity Owner;

    public virtual void Init(Entity owner)
    {
        Owner = owner;
        Owner.OnUpdate += Update;
    }

    protected virtual void Update()
    {
        //
    }
}

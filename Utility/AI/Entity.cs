﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public event Action OnUpdate;

    public TeamDefinition def;

    public List<eComponent> eComps = new List<eComponent>();

    private void Start()
    {
        eComps.Add(def);

        foreach (var k in eComps)
        {
            k.Init(this);
        }
    }

    public T Get<T>() where T : eComponent
    {
        return GetEComponent<T>();
    }

    public T GetEComponent<T>() where T : eComponent
    {
        return eComps.FirstOrDefault((x) => x.GetType() == typeof(T)) as T;
    }

    public virtual void Update()
    {
        OnUpdate?.Invoke();
    }
}

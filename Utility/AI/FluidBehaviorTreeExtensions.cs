﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CleverCrow.Fluid.BTs.Trees;
using System;

public static class FluidBehaviorTreeBuilderExtensions
{
    public static BehaviorTreeBuilder AgentDestination(this BehaviorTreeBuilder builder, string name, Func<Vector3> target)
    {
        return builder.AddNode(new AgentDestination
        {
            Name = name,
            Target = target,
        });
    }
}

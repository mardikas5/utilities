﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TeamDefinition : eComponent
{
    public enum TeamEnum
    {
        Enemy,
        Player
    }

    public TeamEnum Team;
}

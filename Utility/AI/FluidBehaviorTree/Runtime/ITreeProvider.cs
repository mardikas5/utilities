﻿using CleverCrow.Fluid.BTs.Trees;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITreeProvider
{
    BehaviorTree i_BehaviorTree { get; }

    BehaviorTree PreviewBuild();
}

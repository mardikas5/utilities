﻿using CleverCrow.Fluid.BTs.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScoredTask : ITask
{
    float i_Score { get; }
    bool i_IsScored { get; }
}

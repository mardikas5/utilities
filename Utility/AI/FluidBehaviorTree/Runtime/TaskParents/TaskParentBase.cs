﻿using System;
using System.Collections.Generic;
using CleverCrow.Fluid.BTs.Tasks;
using CleverCrow.Fluid.BTs.Trees;
using UnityEngine;

namespace CleverCrow.Fluid.BTs.TaskParents {
    public abstract class TaskParentBase : GenericTaskBase, ITaskParent, IScoredTask {

        [Serializable]
        public class ScoreBehavior
        {
            public bool IsScored = false;
            /// <summary>
            /// The higher the score, the more priority.
            /// </summary>
            public float Score = -1f;

            public ScoreBehavior(bool isScored, float score)
            {
                IsScored = isScored;
                Score = score;
            }

            public ScoreBehavior() { }
        }

        private int _lastTickCount;

        public Func<ScoreBehavior> GetScore;

        #region score interface
        public float i_Score { get => GetScore?.Invoke().Score ?? -1f; } //set { if (GetScore == null) { return; } Scored.Score = value; } }
        public bool i_IsScored { get => GetScore?.Invoke().IsScored ?? false; } //set { if (GetScore == null) { return; } Scored.IsScored = value; } }
        #endregion

        public IBehaviorTree ParentTree { get; set; }
        public TaskStatus LastStatus { get; private set; }

        public virtual string Name { get; set; }
        public bool Enabled { get; set; } = true;

        public List<ITask> Children { get; } = new List<ITask>();

        protected virtual int MaxChildren { get; } = -1;

        public GameObject Owner { get; set; }

        public override TaskStatus Update () {
            base.Update();
            UpdateTicks();

            var status = OnUpdate();
            LastStatus = status;

            return status;
        }

        protected virtual void UpdateTicks () {
            if (ParentTree == null) {
                return;
            }
            
            if (_lastTickCount != ParentTree.TickCount) {
                Reset();
            }

            _lastTickCount = ParentTree.TickCount;
        }

        public virtual void End () {
            throw new System.NotImplementedException();
        }

        protected virtual TaskStatus OnUpdate () {
            return TaskStatus.Success;
        }

        public virtual void Reset () {
        }

        public virtual ITaskParent AddChild (ITask child) {
            if (!child.Enabled) {
                return this;
            }
            
            if (Children.Count < MaxChildren || MaxChildren < 0) {
                Children.Add(child);
            }

            return this;
        }
    }
}
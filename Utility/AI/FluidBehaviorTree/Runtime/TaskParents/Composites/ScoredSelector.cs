﻿using CleverCrow.Fluid.BTs.TaskParents.Composites;
using CleverCrow.Fluid.BTs.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoredSelector : Selector
{


    protected override void UpdateTicks()
    {
        base.UpdateTicks();

        ReorderChildren();
    }

    public virtual void ReorderChildren()
    {
        List<ITask> storedChildren = Children;
        List<IScoredTask> newOrder = new List<IScoredTask>();

        for (int i = 0; i < Children.Count; i++)
        {
            IScoredTask t = Children[i] as IScoredTask;

            if (t != null)
            {
                newOrder.Add(t);
            }
        }

        newOrder = newOrder.OrderByDescending((x) => x.i_Score).ToList();

        int scoredIndex = 0;

        for (int i = 0; i < storedChildren.Count;i++)
        {
            IScoredTask t = storedChildren[i] as IScoredTask;
            //if its a scored child.
            if (t != null)
            {
                Children[i] = newOrder[scoredIndex];
                scoredIndex++;
                continue;
            }
            //if its a static child.
            Children[i] = storedChildren[i];
        }
    }
}

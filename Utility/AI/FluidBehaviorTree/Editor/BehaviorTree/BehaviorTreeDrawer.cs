using UnityEditor;
using UnityEngine;

namespace CleverCrow.Fluid.BTs.Trees.Editors
{
    [CustomPropertyDrawer(typeof(BehaviorTree))]
    public class BehaviorTreeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            position.height -= 25f;

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            if (GUI.Button(position, "View Compile Time Tree"))
            {
                ViewPreviewTree(property);
            }

            position.y += 25f;

            GUI.enabled = Application.isPlaying;
            if (GUI.Button(position, "View Tree"))
            {
                IBehaviorTree tree = fieldInfo.GetValue(property.serializedObject.targetObject) as IBehaviorTree;
                BehaviorTreeWindow.ShowTree(tree, tree.Name ?? property.displayName);
            }
            GUI.enabled = true;

            EditorGUI.EndProperty();
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 50f;
            //return base.GetPropertyHeight(property, label);
        }

        public void ViewPreviewTree(SerializedProperty property)
        {
            ITreeProvider k = property.serializedObject.targetObject as ITreeProvider;
            object propertyObject = GetPropertyObject(property);
            BehaviorTree testTree = null;
            if (k != null)
            {
                {
                    testTree = k.PreviewBuild();
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                Debug.LogError("Implement ITreePreviewProvider to view a preview");
                return;
            }

            if (testTree == null)
            {
                Debug.LogError(" null tree!! ");
            }

            if (testTree.Root == null)
            {
                Debug.LogError(" null root!! ");
            }

            BehaviorTreeWindow.ShowTree(testTree, testTree.Name ?? property.displayName);
        }

        //???? digusting.
        public object GetPropertyObject(SerializedProperty prop)
        {
            string[] path = prop.propertyPath.Split('.');
            object propertyObject = prop.serializedObject.targetObject;
            foreach (string pathNode in path)
            {
                propertyObject = propertyObject.GetType().GetField(pathNode).GetValue(propertyObject);
            }

            return propertyObject as object;
        }
    }



}

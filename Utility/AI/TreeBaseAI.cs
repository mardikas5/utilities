﻿using CleverCrow.Fluid.BTs.Trees;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[SerializeField]
public class TreeBaseAI : MonoBehaviour, ITreeProvider
{
    [Serializable]
    public class BehaviourTreeContainer : ITreeProvider
    {
        public List<Object> ChildTrees = new List<Object>();

        public GameObject gameObject;

        [HideInInspector]
        public BehaviorTree ContainerActiveTree;

        public BehaviorTree i_BehaviorTree => ContainerActiveTree;

        public BehaviorTree PreviewBuild(GameObject gameObject)
        {
            this.gameObject = gameObject;

            return PreviewBuild();
        }

        public BehaviorTreeBuilder GetBuilder(GameObject gameObject, string name = "")
        {
            BehaviorTreeBuilder newTreeBuilder = new BehaviorTreeBuilder(gameObject).Name(name);

            for (int i = 0; i < ChildTrees.Count; i++)
            {
                if (ChildTrees[i] == null) { continue; }
                ITreeProvider provider = TryGetProvider(ChildTrees[i]);
                if (provider == null) { continue; }
                var k = provider.PreviewBuild();
                k.Name = "Spliced: " + k.Name;
                newTreeBuilder.Splice(k);
            }

            return newTreeBuilder;
        }

        private ITreeProvider TryGetProvider(Object obj)
        {
            ITreeProvider provider = obj as ITreeProvider;
            if (provider == null)
            {
                GameObject _provider = obj as GameObject;
                provider = _provider.GetComponent<ITreeProvider>();
            }
            return provider;
        }

        public BehaviorTree PreviewBuild()
        {
            BehaviorTree k = GetBuilder(gameObject).Build("testor");

            ContainerActiveTree = k;

            return k;
        }
    }


    public BehaviourTreeContainer TreeContainer;

    public BehaviorTree ActiveTree;

    public BehaviorTree i_BehaviorTree => ActiveTree;

    public virtual string RootName => "BaseAI";

    private void Awake()
    {
        PreviewBuild();
    }

    public virtual BehaviorTree PreviewBuild()
    {
        ActiveTree = TreeBuilder().Build(RootName);
        return ActiveTree;
    }

    /// <summary>
    /// Main method to create your behaviour tree.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    protected virtual BehaviorTreeBuilder TreeBuilder(string name = "base")
    {
        return TreeContainer.GetBuilder(gameObject, name);
    }

    public virtual void Update()
    {
        ActiveTree.Tick();
    }

}

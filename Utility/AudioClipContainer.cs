﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipContainer : MonoBehaviour
{
    public bool AdjustPitch = false;
    public Vector2 RandPitch = new Vector2(1,1);

    public List<AudioClip> clips = new List<AudioClip>();

    public bool PlayOnAwake = false;
    [SerializeField]
    private bool destroyAfterPlay = false;

    private void Awake()
    {
        PlayOnAwakeHandler();
    }

    private void PlayOnAwakeHandler()
    {
        if (!PlayOnAwake)
        {
            return;
        }

        AudioSource source = null;
        if (source = GetComponent<AudioSource>())
        {
            PlayRandom(source);
        }
    }

    public void PlayRandom()
    {
        //AudioSource source = GetComponent<AudioSource>();

        var copy = Instantiate(gameObject);
        copy.transform.parent = transform.parent.GetChild(0);
        copy.name = "Copy";

        AudioClipContainer container = copy.GetComponent<AudioClipContainer>();
        container.destroyAfterPlay = true;
        AudioSource source = copy.GetComponent<AudioSource>();
        container.PlayRandom(source);
    }

    public void PlayRandom(AudioSource source)
    {
        if (source == null)
        {
            return;
        }
        if (AdjustPitch)
        {
            source.pitch = RandPitch.Rand();
        }
        var clip = clips.Random();
        float length = clip.length / source.pitch;
        source.PlayOneShot(clip);
        if (destroyAfterPlay)
        {
            this.InvokeDelayed(length + .1f, () => Destroy(gameObject));
        }
    }
}

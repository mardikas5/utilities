﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class GetWeightedRandom
{
    public static T GetObject<T>(List<T> allObjects, Func<T, float> getWeight) where T : class
    {
        float totalWeight = 0f;
        allObjects.ForEach((x) => totalWeight += getWeight(x));
        float randomNr = Random.Range(0f, totalWeight);
        float currentWeight = 0f;

        for (int i = 0;i < allObjects.Count; i++)
        {
            currentWeight += getWeight(allObjects[i]);
            if (currentWeight > randomNr)
            {
                return allObjects[i];
            }
        }

        return allObjects[allObjects.Count - 1];
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MonoUtility
{
    public static Coroutine InvokeDelayed(this MonoBehaviour behaviour, float time, Action action)
    {
        return behaviour.StartCoroutine(InvokeAfterTime(time, action));
    }

    public static Coroutine InvokeRepeating(this MonoBehaviour behaviour, float time, Action action)
    {
        return behaviour.StartCoroutine(InvokeRepeating(time, action));
    }

    public static IEnumerator InvokeAfterTime(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }

    public static IEnumerator InvokeRepeating(float time, Action action)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            action?.Invoke();
        }
    }

}

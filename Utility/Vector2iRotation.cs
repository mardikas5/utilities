﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Vector2iRotation
{
    public static readonly Vector2iRotation identity = new Vector2iRotation( 0 );

    /// <summary>
    /// Number ranging from 0-3 representing each rotation increment in 90 degrees
    /// </summary>
    public int N { get; private set; }

    private int xIndex;
    private int yIndex;

    private int xSign;
    private int ySign;
    
    public Vector2iRotation( int N )
    {
        this.N = MathUtility.Mod( N, 4 );

        xIndex = 0;
        yIndex = 0;
        xSign = 0;
        ySign = 0;

        UpdateIndices();
    }

    public void UpdateIndices()
    {
        xIndex = 0;
        yIndex = 1;

        xSign = 1;
        ySign = 1;

        for( int i = 0; i < N; ++i )
        {
            MathUtility.Swap( ref xIndex, ref yIndex );

            MathUtility.Swap( ref xSign, ref ySign );

            ySign *= -1;
        }
    }

    public Quaternion ToQuaternion()
    {
        return Quaternion.Euler( 0.0f, N * 90.0f, 0.0f );
    }

    public void RotateRight()
    {
        N = ( N + 1 ) % 4;

        UpdateIndices();
    }

    public void Rotate180()
    {
        N = ( N + 2 ) % 4;

        UpdateIndices();
    }

    public void RotateLeft()
    {
        N = ( N + 3 ) % 4;

        UpdateIndices();
    }

    public Vector2i ToDirection()
    {
        Vector2i result = this * Vector2i.up;

        return result;
    }

    public static Vector2i operator *( Vector2iRotation rotation, Vector2i v )
    {
        return new Vector2i( v[rotation.xIndex] * rotation.xSign, v[rotation.yIndex] * rotation.ySign );
    }

    public static Vector2 operator *( Vector2iRotation rotation, Vector2 v )
    {
        return new Vector2( v[rotation.xIndex] * (float)rotation.xSign, v[rotation.yIndex] * (float)rotation.ySign );
    }

    public static Side operator *( Vector2iRotation rotation, Side s )
    {
        int a = ( (int)s + rotation.N ) % 4;

        return (Side)a;
    }

    public static Vector2iRotation operator +( Vector2iRotation rotation, Vector2iRotation other )
    {
        return new Vector2iRotation( rotation.N + other.N );
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnDelay : MonoBehaviour
{
    public GameObject Obj;
    public float Delay;
    // Start is called before the first frame update
    void Start()
    {
        this.InvokeDelayed(Delay, () => Obj?.SetActive(true));
    }
}

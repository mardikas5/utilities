﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDelay : MonoBehaviour
{
    public float Delay;
    public GameObject SpawnObject;
        
    void Start()
    {
        this.InvokeDelayed(Delay, DoSpawn);
    }

    void DoSpawn()
    {
        Instantiate(SpawnObject, transform.position, Quaternion.identity, null);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerListener : MonoBehaviour
{
    public event Action<Collider> OnEnter;
    public event Action<Collider2D> OnEnter2D;
    public event Action<Collider> OnExit;
    public event Action<Collider2D> OnExit2D;

    public event Action<Collision2D> OnEnterCollision2D;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnEnter2D?.Invoke(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        OnExit2D?.Invoke(collision);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnEnter2D?.Invoke(collision.collider);
        OnEnterCollision2D?.Invoke(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        OnExit2D?.Invoke(collision.collider);
    }

    private void OnTriggerEnter(Collider other)
    {
        OnEnter?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        OnExit?.Invoke(other);
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnEnter?.Invoke(collision.collider);
    }

    private void OnCollisionExit(Collision collision)
    {
        OnExit?.Invoke(collision.collider);
    }
}

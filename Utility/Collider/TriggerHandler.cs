﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TriggerHandler
{
    public List<TriggerListener> HandleTriggers = new List<TriggerListener>();

    public event Action<Collider> onEnter;

    public void Init()
    {
        foreach (TriggerListener k in HandleTriggers)
        {
            if (k == null)
            {
                continue;
            }
            k.OnEnter += (x) => onEnter?.Invoke(x);
        }
    }
}

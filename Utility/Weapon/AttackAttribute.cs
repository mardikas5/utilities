﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AttackAttribute
{
    public Attack m_BaseAttack;
}

[Serializable]
public class BlockAttribute : AttackAttribute
{
    public bool IsBlockable = true;
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Attack : MonoBehaviour, IDamageSource
{
    public static int[] hitLayers;
    private static void InitHitLayers()
    {
        if (hitLayers != null && hitLayers.Length >= 0)
        {
            return;
        }

        hitLayers = new int[] { LayerMask.NameToLayer("HitBox"), LayerMask.NameToLayer("Default") };
    }

    public bool UseHitLayers = false;
    public bool overrideHitLayers = false;
    public int[] overrideHitLayersValues;

    public bool DestroyOnHit = true;

    public Vector3 StartPos;

    public Vector3 Direction;

    public WeaponSystem.Weapon Weapon;

    public List<GameObject> hitobjects = new List<GameObject>();

    public float spawnTime;

    public event Action<Hit> OnProjectileHit;

    public List<Func<Hit,bool>> HitChecks = new List<Func<Hit,bool>>();

    public List<AttackAttribute> Attributes = new List<AttackAttribute>();

    public UnityEvent Unity_OnProjectileHit;

    public GameObject spawnOnHit;

    public Transform deparent;

    //Interface
    public float i_Amount { get => m_DamageAmount; set => m_DamageAmount = value; }
    private float m_DamageAmount;

    /// <summary>
    /// If check returns false, the hit does not count.
    /// </summary>
    /// <param name="check"></param>
    public void RegisterCheck(Func<Hit, bool> check)
    {
        HitChecks.Add(check);
    }

    public virtual void Init(Vector3 dir, Vector3 startPos, WeaponSystem.Weapon weapon)
    {
        hitobjects.Clear();
        Weapon = weapon;
        Direction = dir.normalized;
        StartPos = startPos;
        spawnTime = Time.time;

        m_DamageAmount = weapon.Damage;
        RegisterCheck(HitOwner);
        InitHitLayers();
    }

    protected virtual bool HitOwner(Hit hitObj)
    {
        if (hitObj.transform.GetComponentInChildren<WeaponSystem.Weapon>()?.Owner == Weapon?.Owner)
        {
            return false;
        }

        return true;
    }

    public virtual T GetAttribute<T>() where T : AttackAttribute
    {
        return Attributes.FirstOrDefault((x) => x.GetType() == typeof(T)) as T;
    }

    public virtual bool RegisterAttribute<T>(T Attribute) where T : AttackAttribute
    {
        Attributes.Add(Attribute);
        Attribute.m_BaseAttack = this;
        return true;
    }

    public virtual void OnHitOther(Collider2D other)
    {
        Hit hit = new Hit();

        if (UseHitLayers)
        {
            if (!IsHitLayer(other))
            {
                return;
            }
        }

        hit.transform = GetHitTransform(other);

        if (Weapon.OnlyHitOnce)
        {
            if (hitobjects.Contains(hit.transform.gameObject))
            {
                return;
            }
            hitobjects.Add(hit.transform.gameObject);
        }

        hit.weapon = Weapon;

        foreach (Func<Hit, bool> k in HitChecks)
        {
            if (k == null)
            {
                continue;
            }

            if (!k.Invoke(hit))
            {
                return;
            }
        }

        IHittable[] hittables = hit.transform.GetComponentsInChildren<IHittable>();

        if (hittables != null)
        {
            foreach (IHittable t in hittables)
            {
                t.TakeDamage(hit.weapon.Damage);
            }
        }
        
        //Hit any object, wall, character etc.
        OnHit(hit);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        OnHitOther(other);
    }

    protected virtual bool IsHitLayer(Collider2D other)
    {
        if (overrideHitLayers)
        {
            return overrideHitLayersValues.Any((x) => x == other.gameObject.layer);
        }
        return hitLayers.Any((x) => x == other.gameObject.layer);
    }

    //private Character GetHitChar(Collider other)
    //{
    //    Character character = other?.attachedRigidbody?.GetComponentInChildren<Character>();

    //    if (character == null)
    //    {
    //        character = other?.GetComponentInChildren<Character>();
    //    }

    //    return character;
    //}

    private Transform GetHitTransform(Collider2D other)
    {
        Transform hitTransform = other?.attachedRigidbody?.transform;

        if (hitTransform == null)
        {
            hitTransform = other.transform;
        }

        return hitTransform;
    }

    protected virtual void OnHit(Hit hit)
    {
        if (hit != null)
        {
            if (spawnOnHit != null)
            {
                Instantiate(spawnOnHit, transform.position, Quaternion.identity, null);
            }
        }

        OnProjectileHit?.Invoke(hit);
        Unity_OnProjectileHit?.Invoke();
        if (deparent != null)
        {
            deparent.SetParent(null);
        }

        if (DestroyOnHit)
        {
            Destroy(gameObject);
        }
    }
}

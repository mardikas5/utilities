﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Hit
{
    public WeaponSystem.Weapon weapon;
    public Transform transform;
    //public Character character;
}

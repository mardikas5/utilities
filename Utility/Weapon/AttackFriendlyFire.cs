﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WeaponSystem;
using static Attack;

public class AttackFriendlyFire : MonoBehaviour
{
    public void Start()
    {
        GetComponent<Attack>()?.RegisterCheck(IsFriendlyFire);
    }

    protected virtual bool IsFriendlyFire(Hit hit)
    {
        if (hit.weapon == null)
        {
            return false;
        }

        WeaponFriendlyFire FriendlyFire = hit.weapon.GetComponent<WeaponFriendlyFire>();

        if (FriendlyFire == null)
        {
            return true;
        }

        if (!FriendlyFire.FFActive)
        {
            //if (FriendlyFire?.Team == null)
            {
                //return false;
            }
            //if (hit.character?.Team == FriendlyFire?.Team)
            {
                //return false;
            }
        }

        return true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Speed;
    public Rigidbody2D rb;


    // Update is called once per frame
    void Update()
    {
        rb.MovePosition(GetPos());    
    }

    public Vector2 GetPos()
    {
        return transform.position + (transform.right * Speed * Time.deltaTime);
    }
}

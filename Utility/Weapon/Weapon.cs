﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//bad namespace n name combo
namespace WeaponSystem
{
    public class Weapon : MonoBehaviour
    {
        [Header("Weapon")]
        public float Damage;

        public float Range = 25f;

        public bool OnlyHitOnce;

        public float castCoolDown;

        public float attackDuration;

        public float spawnAttackDelay;

        public GameObject PreAttackFX;

        public Attack AttackFX;

        [NonSerialized]
        public List<Func<CustomEvent<bool>, bool>> fireConditions = new List<Func<CustomEvent<bool>,bool>>();

        public event Action OnFire;
        public event Action OnFireEnd;

        public Transform Owner;
        public bool UseOwnerDir;

        protected float nextFireTime;

        private CustomEvent<bool> fireCallback = new CustomEvent<bool>();

        protected List<Attack> ActiveAttacks = new List<Attack>();

        private void Start()
        {
        }

        public virtual float GetDamage()
        {
            return Damage;
        }

        public virtual bool TryFire(Vector3 dir)
        {
            if (UseOwnerDir)
            {
                dir = AttackFX.transform.forward;
            }

            fireCallback.ClearCallbacks();

            if (Time.time < nextFireTime)
            {
                return false;
            }

            if (dir == Vector3.zero)
            {
                return false;
            }

            foreach (Func<CustomEvent<bool>, bool> fireCondition in fireConditions)
            {
                if (!fireCondition(fireCallback))
                {
                    fireCallback?.Invoke(false);
                    return false;
                }
            }

            fireCallback?.Invoke(true);

            OnPreFire(dir);

            return true;
        }

        protected virtual void OnPreFire(Vector3 dir)
        {
            if (PreAttackFX != null)
            {
                Instantiate(PreAttackFX, transform.position, Quaternion.identity, null);
            }

            nextFireTime = Time.time + castCoolDown + spawnAttackDelay;

            this.InvokeDelayed(spawnAttackDelay,() => Fire(dir, out Attack atk));
        }

        public virtual void Fire(Vector3 dir, out Attack atk)
        {
            atk = null;

            if (AttackFX != null)
            {
                atk = Instantiate(AttackFX, transform.position, Quaternion.identity, null);
                atk.transform.right = dir;
                atk.Init(dir, transform.position, this);

                ActiveAttacks.Add(atk);
            }

            OnFire?.Invoke();

            this.InvokeDelayed(attackDuration, () => OnFireEnd?.Invoke());
        }

        public void AimAt(Transform target)
        {
            transform.forward = target.transform.position.x0z() - transform.position.x0z();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponSystem
{
    
    public class WeaponFriendlyFire : MonoBehaviour
    {
        //[Header("FriendlyFire")]
        //public Character.TeamEnum? Team;

        public bool FFActive = false;

        public Weapon Weapon;

        private void Start()
        {
            Weapon = GetComponent<Weapon>();
            //Team = Weapon.Owner.GetComponent<Character>()?.Team;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDestroy : MonoBehaviour
{
    public GameObject SpawnObj;
    public Transform spawnToPos;

    private void OnDestroy()
    {
        if (spawnToPos == null)
        {
            spawnToPos = transform;
        }

        if (SpawnObj == null)
        {
            return;
        }

        Instantiate(SpawnObj, spawnToPos.position, Quaternion.identity, null);
    }
}

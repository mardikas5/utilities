﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public enum ShakeTypeEnum
    {
        Angle,
        Position
    }

    public Transform _Camera;
    public ShakeTypeEnum ShakeEnum;
    public float Multiplier = 1f;

    public void Awake()
    {
        if (_Camera == null)
        {
            //_Camera = GetComponent<Camera>();
        }
    }

    public void AddShake(float magnitude, int freq, float time)
    {
        magnitude *= Multiplier;
        if (ShakeEnum == ShakeTypeEnum.Angle)
        {
            Tweener t = _Camera.DOShakeRotation(time, magnitude, freq);
            t.OnComplete(() => _Camera.transform.DOLocalRotate(Vector3.zero, .1f));
        }
        if (ShakeEnum == ShakeTypeEnum.Position)
        {
            magnitude *= 5f;
            Tweener t = _Camera.DOShakePosition(time, magnitude, freq);
            t.OnComplete(() => _Camera.transform.DOLocalMove(Vector3.zero, .1f));
        }
    }
}

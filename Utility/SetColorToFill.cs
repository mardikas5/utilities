﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetColorToFill : MonoBehaviour
{
    public Image Img;

    public Color ZeroFill;
    public Color OneFill;

    public float LastUpdateFill;

    private void Start()
    {
        if (Img == null)
        {
            Img = GetComponent<Image>();
        }
    }

    void UpdateImage()
    {
        if (Img.fillAmount == LastUpdateFill)
        {
            return;
        }

        float fill = Img.fillAmount;

        LastUpdateFill = Img.fillAmount;

        Img.color = (OneFill * fill) + (ZeroFill * (1f - fill));

    }

    // Update is called once per frame
    void Update()
    {
        UpdateImage();
    }
}

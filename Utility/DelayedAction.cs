﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedAction : DelayedAction<object>
{
    public new event Action Callback;

    public DelayedAction(Action callback = null) : base()
    {
        if (callback == null)
        {
            return;
        }

        Callback += callback;
    }

    public void Invoke()
    {
        Callback?.Invoke();
    }
}

public class DelayedAction<T>
{
    public event Action<T> Callback;

    protected DelayedAction() { }

    public DelayedAction(Action<T> callback)
    {
        Callback += callback;
    }

    public DelayedAction<T> OnInvoke(Action<T> a)
    {
        Callback += a;
        return this;
    }

    public void Invoke(T value)
    {
        Callback?.Invoke(value);
    }
}

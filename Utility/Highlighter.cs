﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Highlighter
{
    private Dictionary<Renderer, Material> _SavedRendererMaterials = new Dictionary<Renderer, Material>();

    private Material highLight;


    public bool HighlightAllChildren = true;

    public List<Renderer> highlight = new List<Renderer>();


    public void Init(Material highlightMat)
    {
        highLight = highlightMat;
    }

    public void Apply(GameObject obj)
    {
        Renderer[] rends = obj.GetComponentsInChildren<Renderer>();
        Array.ForEach(rends, (x) =>
        {
            if (!_SavedRendererMaterials.ContainsKey(x))
            { _SavedRendererMaterials.Add(x, x.sharedMaterial); }
        });
        Array.ForEach(rends, (x) => x.sharedMaterial = highLight);
    }

    public void Remove()
    {
        _SavedRendererMaterials.ToList().ForEach((x) => x.Key.material = x.Value);
        _SavedRendererMaterials.Clear();
    }
}

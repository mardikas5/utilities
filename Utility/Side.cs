﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Side
{
    North,
    East,
    South,
    West
}

[System.Flags]
public enum SideMask : uint
{
    None = 0,
    North = 1,
    East = 2,
    South = 4,
    West = 8
}

public static class SideMethods
{
    public static Side Opposite( this Side side )
    {
        switch( side )
        {
            default:
            case Side.North:
                return Side.South;
            case Side.East:
                return Side.West;
            case Side.South:
                return Side.North;
            case Side.West:
                return Side.East;
        }
    }

    public static Vector2i ToDirection( this Side side )
    {
        switch( side )
        {
            default:
            case Side.North:
                return Vector2i.up;
            case Side.East:
                return Vector2i.right;
            case Side.South:
                return Vector2i.down;
            case Side.West:
                return Vector2i.left;
        }
    }

    public static Vector2iRotation ToRotation( this Side side )
    {
        return new Vector2iRotation( (int)side );
    }

    public static int Axis( this Side side )
    {
        switch( side )
        {
            default:
            case Side.South:
            case Side.North:
                return 1;
            case Side.West:
            case Side.East:
                return 0;
        }
    }

    public static int TangentAxis( this Side side )
    {
        switch( side )
        {
            default:
            case Side.South:
            case Side.North:
                return 0;
            case Side.West:
            case Side.East:
                return 1;
        }
    }

    public static Side RotateCCW( this Side side )
    {
        switch( side )
        {
            default:
            case Side.North:
                return Side.West;
            case Side.East:
                return Side.North;
            case Side.South:
                return Side.East;
            case Side.West:
                return Side.South;
        }
    }

    public static Side RotateCW( this Side side )
    {
        switch( side )
        {
            default:
            case Side.North:
                return Side.East;
            case Side.East:
                return Side.South;
            case Side.South:
                return Side.West;
            case Side.West:
                return Side.North;
        }
    }
}
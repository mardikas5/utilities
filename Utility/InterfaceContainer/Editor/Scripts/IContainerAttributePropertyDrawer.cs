﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

//wip not working.
[CustomPropertyDrawer(typeof(IContainerAttribute))]
public class IContainerAttributePropertyDrawer : PropertyDrawer {

    private Object pickerObject = null;
    private Component[] objs;
    private SerializedProperty _prop;
    private Behaviour Container;

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        IContainerAttribute at = attribute as IContainerAttribute;
        
        //int indent = EditorGUI.indentLevel;
        //EditorGUI.indentLevel = 0;

        //try {
        //    Container = property.serializedObject.targetObject as Behaviour;

        //    Draw(property);
        //}
        ////unity nice things happening, need the try catch.
        //catch (Exception e) {
        //    Debug.LogWarning(e.ToString());
        //}

        //EditorGUI.indentLevel = indent;

    }

    private void Draw(SerializedProperty property) {
        GUILayout.BeginVertical("HelpBox");
        Type t = GetType(property).GenericTypeArguments[0];

        string content = $"{property.name} - Type : {t?.Name}";
        SerializedProperty interfaceRef = property.FindPropertyRelative("_interface");
        _prop = interfaceRef;
        EditorGUILayout.LabelField(content);

        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 1f;
        EditorGUILayout.ObjectField(interfaceRef);
        EditorGUI.EndDisabledGroup();
        DoObjectPicker(t, interfaceRef);

        EditorGUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    private void ShowList(SerializedProperty property, Rect pos) {
        PopupPicker window = Editor.CreateInstance<PopupPicker>();
        window = window.Open(objs);
        window.position = pos;
        window.OnGetValue += OnGetValue;
    }

    private void OnGetValue(Component t) {
        _prop.objectReferenceValue = t;
        _prop.serializedObject.ApplyModifiedProperties();
    }

    private void DoObjectPicker(Type t, SerializedProperty property) {
        if (GUILayout.Button("Pick", GUILayout.Width(50f))) {
            int controlID = EditorGUIUtility.GetControlID (FocusType.Passive);
            EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, "", controlID);
        }

        if (GUILayout.Button("Self", GUILayout.Width(50f))) {
            OnObjectPicked(t, property, Container.gameObject);
        }

        if (property?.objectReferenceValue != null) {
            if (GUILayout.Button("X", GUILayout.Width(25f))) {
                property.objectReferenceValue = null;
            }
        }

        string commandName = Event.current.commandName;

        if (commandName == "ObjectSelectorClosed") {
            OnObjectPicked(t, property, EditorGUIUtility.GetObjectPickerObject() as GameObject);
        }
    }

    private void OnObjectPicked(Type t, SerializedProperty property, GameObject pickerObject) {
        //pickerObject = EditorGUIUtility.GetObjectPickerObject();
        objs = ((GameObject)pickerObject).GetComponents(t);
        ShowList(property, new Rect(Screen.width / 2f, Screen.height / 2f, 300f, 200f));
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return 25f;// EditorGUI.GetPropertyHeight(property);
    }

    public static System.Type GetType(SerializedProperty property) {
        System.Type parentType = property.serializedObject.targetObject.GetType();
        BindingFlags flags =  BindingFlags.NonPublic | BindingFlags.Instance;
        System.Reflection.FieldInfo fi = parentType?.GetField(property.propertyPath, flags);
        return fi?.FieldType;
    }
}

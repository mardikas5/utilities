﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class PopupPicker : EditorWindow {


    private static EditorWindow window;

    private Vector2 scrollPos = Vector2.zero;

    public Component[] objs;

    public event Action<Component> OnGetValue;

    private static PopupPicker Init(Component[] objs) {
        if (window != null) {
            window.Close();
            window = null;
        }

        window = CreateInstance<PopupPicker>();
        ((PopupPicker)window).objs = objs;
        window.Show();
        return window as PopupPicker;
    }

    private void OnGUI() {
        EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        string delimiter = " | ";
        for (int i = 0; i < objs.Length; i++) {
            string[] implements = objs[i].GetType().GetInterfaces().Select((x) => x.ToString()).ToArray();
            string buttonString = $"{objs[i].name} - {objs[i].GetType().ToString()} \n {implements.Aggregate((all, next) => all + delimiter + next)}";
            if (GUILayout.Button(buttonString)) {
                OnGetValue?.Invoke(objs[i]);
                window.Close();
            }
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }

    public PopupPicker Open(Component[] objs) {
        return Init(objs);
    }
}

#endif
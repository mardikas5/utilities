﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

[CustomPropertyDrawer(typeof(InterfaceContainer<>))]
public class InterfaceContainerEditor : PropertyDrawer {

    private Object pickerObject = null;
    private Component[] objs;
    private SerializedProperty _prop;
    private Behaviour Container;
    private Vector2 mousePos;
    private Rect lastRect;

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        lastRect = GUILayoutUtility.GetLastRect();
        mousePos = Event.current.mousePosition;

        try {
            Container = property.serializedObject.targetObject as Behaviour;

            Draw(property);
        }
        //unity nice things happening, need the try catch.
        catch (Exception e) {
            Debug.LogWarning(e.ToString());
        }

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    private void Draw(SerializedProperty property) {

        Type t = GetType(property).GenericTypeArguments[0];

        string content = $"{property.name} - Type : {t?.Name}";
        SerializedProperty interfaceRef = property.FindPropertyRelative("_interface");
        _prop = interfaceRef;

        if (interfaceRef == null) {
            EditorGUILayout.LabelField("editor script error, property not found");
            return;
        }

        GUILayout.BeginVertical("HelpBox");

        EditorGUILayout.LabelField(content);

        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginDisabledGroup(true);
        EditorGUIUtility.labelWidth = 1f;
        EditorGUILayout.ObjectField(interfaceRef);
        EditorGUI.EndDisabledGroup();
        DoObjectPicker(t, interfaceRef);

        EditorGUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    private void ShowList(SerializedProperty property, Rect pos) {
        PopupPicker window = Editor.CreateInstance<PopupPicker>();
        window = window.Open(objs);
        window.position = pos;
        window.OnGetValue += OnGetValue;
    }

    private void OnGetValue(Component t) {
        _prop.objectReferenceValue = t;
        _prop.serializedObject.ApplyModifiedProperties();
    }

    private void DoObjectPicker(Type t, SerializedProperty property) {
        int controlID = EditorGUIUtility.GetControlID (FocusType.Passive);

        if (GUILayout.Button("Pick", GUILayout.Width(50f))) {
            EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, "", controlID);
        }

        if (GUILayout.Button("Self", GUILayout.Width(50f))) {
            OnObjectPicked(t, property, Container.gameObject);
        }

        if (property?.objectReferenceValue != null) {
            if (GUILayout.Button("X", GUILayout.Width(25f))) {
                OnGetValue(null);
            }
        }

        if (EditorGUIUtility.GetObjectPickerControlID() != controlID) {
            return;
        }

        string commandName = Event.current.commandName;

        if (commandName == "ObjectSelectorClosed") {
            OnObjectPicked(t, property, EditorGUIUtility.GetObjectPickerObject() as GameObject);
        }
    }

    private void OnObjectPicked(Type t, SerializedProperty property, GameObject pickerObject) {
        if (pickerObject == null) {
            return;
        }

        lastRect = GUILayoutUtility.GetLastRect();
        Vector2 pos = GUIUtility.GUIToScreenPoint(lastRect.position);
        float width = 300f;
        pos += mousePos;
        objs = ((GameObject)pickerObject).GetComponents(t);
        ShowList(property, new Rect(pos.x - width, pos.y, width, 200f));
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return -5f;// EditorGUI.GetPropertyHeight(property);
    }

    public static System.Type GetType(SerializedProperty property) {
        System.Type parentType = property.serializedObject.targetObject.GetType();
        BindingFlags flags =  BindingFlags.NonPublic | BindingFlags.Instance;
        System.Reflection.FieldInfo fi = parentType?.GetField(property.propertyPath, flags);
        return fi?.FieldType;
    }
}

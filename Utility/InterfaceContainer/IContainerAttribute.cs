﻿using System;
using UnityEngine;

//wip not working.
[Serializable]
[AttributeUsage(AttributeTargets.Field)]
public class IContainerAttribute : PropertyAttribute {

    public Type InterfaceType { get; private set; }

    public IContainerAttribute(Type type) {
        InterfaceType = type;
    }

    [SerializeField]
    public InterfaceContainer<object> Container;
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class InterfaceContainer<T> where T : class {

    [SerializeField]
    private Behaviour _interface;

    /// <summary>
    /// Stored interface.
    /// </summary>
    public T Interface => GetInterface();

    protected virtual T GetInterface() {
        return _interface as T;
    }

    /// <summary>
    /// Implicit conversion to get the underlying interface.
    /// </summary>
    /// <param name="b"></param>
    public static implicit operator T(InterfaceContainer<T> b) => b.Interface;
}

